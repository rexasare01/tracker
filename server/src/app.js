const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');

const app = express();
app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(cors());

app.post('/register', (req, res) => {
  res.status(200).send(`Hello ${req.body.email} ! Thank you for registering.`);
});

app.listen(process.env.PORT || 8081);
